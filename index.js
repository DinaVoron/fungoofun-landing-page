import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

function Logo() {
  return (
    <div className="logo">
      FUNGOOFUN
    </div>
  )
}

const Colors = [
  {
    src: "1.png"
  },
  {
    src: "2.png"
  },
  {
    src: "3.png"
  }
]

const Sizes = [
  {
    src: "s1.png"
  },
  {
    src: "s2.png"
  }
]

// const infoList = [
//   {
//     name: "Размеры",
//     values: Colors
//   },
//   {
//     name: "Цвета",
//     values: Colors
//   }
// ]



function Tagline() {
  return (
    <div className="tagline">
      <span>Стильная</span> защита <br/> для Вашего ноутбука
    </div>
  )
}

function Button() {
  return (
    <div className="button">
      Заказать
    </div>
  )
}

function InfoItem(props) {
  
  const pictList = props.list.map(
    (elem, index) =>
    <img key={index} src={elem.src}></img>
  );

  return (
    <div className="infoItem">
      {pictList}
    </div>
  )
}


function Info() {
  const [isColorFocused, setColorFocus] = React.useState(false);
  const [isSizeFocused, setSizeFocus] = React.useState(false);

  function colorTrueMouse() {
    setColorFocus(true);
  }

  function colorFalseMouse() {
    setColorFocus(false);
  }

  function sizeTrueMouse() {
    setSizeFocus(true);
  }

  function sizeFalseMouse() {
    setSizeFocus(false);
  }

  return (
    <div className="info">
      <div onMouseOver={colorTrueMouse} onMouseOut={colorFalseMouse}>
        Цвета 
        {isColorFocused &&
          <InfoItem list={Colors} />
        }
      </div>
      <div className="size" onMouseOver={sizeTrueMouse} onMouseOut={sizeFalseMouse}> 
        <span>Размеры</span>
        {isSizeFocused &&
          <InfoItem list={Sizes}/>
        }
      </div>
    </div>
  )
}

function Form() {
  return (
    <form className="form">
      <p>Ваш <span>e-mail</span>:
      <input type="text" />
      </p>
    </form>
  )
}

function Content() {
  return (
    <>
    <Logo />
    <Tagline />
    <Form />
    <Button />
    <Info />
    </>
  );
}


root.render(<Content />);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
